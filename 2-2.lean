import tactic.suggest
import tactic.basic


-- https://math.stackexchange.com/questions/138716/i-want-a-clear-explanation-for-the-principle-of-strong-mathematical-induction

def axiom_of_induction (P : ℕ → Prop) : (P 0 ∧ ∀ x : ℕ, P x → P x.succ) → ∀ y : ℕ, P y := sorry  

lemma l_001 {P : ℕ → Prop} : (∀ y : ℕ, P y) ↔ ∀ z m : ℕ, m < z → P m := by {
    fconstructor, focus {
      intro, intro, intro, intro, exact ᾰ m,
    }, focus {
      intro, intro,
      have oo₁ : y < y.succ := nat.lt_succ_self y,
      exact ᾰ y.succ y oo₁,
    },
}

lemma l_002 {P : ℕ → Prop} : (∀ x : ℕ, (∀ m : ℕ, m < x → P m) → P x) → ∀ z m : ℕ, m < z → P m := by {
  intro, intro, induction z, focus {
    intro, intro,
    have o₁ : m < 0 → false := of_to_bool_ff rfl,
    finish,
  }, focus {
    intro, intro,
    have o₁ : m < z_n ∨ m = z_n := by sorry,
    cases o₁, focus {
      exact z_ih m o₁,
    },
    have o₂ : P z_n := ᾰ z_n z_ih,
    rw o₁, exact o₂,
  },
}

def strong_induction (P : ℕ → Prop) : (∀ x : ℕ, (∀ m : ℕ, m < x → P m) → P x) → (∀ y : ℕ, P y) := by {
  intro,
  exact iff.elim_right l_001 (l_002 ᾰ),
}

lemma l_003 {m₀ : ℕ} {P : ℕ → Prop} : 
(∀ m : ℕ, m ≥ m₀ → (∀ m' : ℕ, m₀ ≤ m' ∧ m' < m → P m') → P m) → 
∀ z x : ℕ, x ≥ m₀ ∧ x < z → P x := by {
  intro, intro, induction z, focus {
    intro, intro, 
    have o₁ : ∀ (m' : ℕ), m₀ ≤ m' ∧ m' < x → P m' := by {
      intro, intro, cases ᾰ_1, 
      have oo₁ : x < 0 → false := dec_trivial,
      finish,
    }, cases ᾰ_1,
    exact ᾰ x ᾰ_1_left o₁,
  }, focus {
    intro, intro, cases ᾰ_1,
    have o₁ : x ≤ z_n := nat.le_of_lt_succ ᾰ_1_right,
    have o₂ : x < z_n ∨ x = z_n := lt_or_eq_of_le o₁,
    cases o₂, focus {
      exact z_ih x ⟨ᾰ_1_left, o₂⟩, 
    }, focus {
      rw ← o₂ at z_ih,
      exact ᾰ x ᾰ_1_left z_ih,
    },
  },
}

lemma l_004 {P : ℕ → Prop} {m₀ : ℕ } : (∀ (x : ℕ), x ≥ m₀ → P x) ↔ ∀ (z x : ℕ), x ≥ m₀ ∧ x < z → P x := by {
  fconstructor, focus {finish,}, focus {intro, intro, intro,
    have o₁ : x < x.succ := by fconstructor, 
    exact ᾰ x.succ x ⟨ᾰ_1, o₁⟩,
  }
}

 -- exercise 2.2.5
def e_2_2_5 {m₀ : ℕ} {P : ℕ → Prop} : 
(∀ m : ℕ, m ≥ m₀ → (∀ m' : ℕ, m₀ ≤ m' ∧ m' < m → P m') → P m) → 
∀ x : ℕ, x ≥ m₀ → P x := by {
  intro,
  exact iff.elim_right l_004 (l_003 ᾰ),
}

-- Exercise 2.2.6
def e_2_2_6 {P : ℕ → Prop} {n : ℕ} {pN : P n} {pH : ∀ x : ℕ, P x.succ → P x} :
∀ a : ℕ, a ≤ n → P (a) := by {
  induction n, focus {
    intro, intro,
    have o₁ : a ≤ 0 → a = 0 := max_eq_right,
    have o₂ : a = 0 := o₁ ᾰ,
    rw o₂, exact pN,
  },
  intro, intro,
  cases ᾰ, focus {
    exact pN,
  },
  let q := n_ih a ᾰ_ᾰ, apply q,
  exact pH n_n pN,
}

def axiom_nat_mul_zero {m : ℕ} : 0 * m = 0 := sorry

def axiom_nat_mul_succ {n m : ℕ} : n.succ * m = n * m + m := sorry


lemma l_005 {n m : ℕ} :
 m * n.succ = m * n + m := by {
   induction m,
   focus {
     rw axiom_nat_mul_zero,
     rw axiom_nat_mul_zero,
   },
   have o₁ : m_n.succ * n = m_n * n + n := axiom_nat_mul_succ,
   rw o₁,
   have o₂ : m_n.succ = m_n + 1 := rfl,
   rw o₂,
   have o₃ : m_n * n + n + (m_n + 1) = m_n * n + m_n + (n + 1) := by sorry,
   rw o₃,
   rw ← m_ih,
   rw ← o₂,
   have o₄ : n + 1 = n.succ := rfl,
   rw o₄,
   rw axiom_nat_mul_succ,
} 

-- Exercise 2.3.1
def e_2_3_1 {m n : ℕ} : m * n = n * m := by {
  induction m,
  rw axiom_nat_mul_zero, induction n,
  rw axiom_nat_mul_zero, rw axiom_nat_mul_succ,
  rw ← n_ih,
  rw l_005,
  rw axiom_nat_mul_succ,
  rw m_ih,
}

lemma l_006 : ∀ m : ℕ, m > 0 → ∃ n : ℕ, m = n.succ := by {
  intro,
  induction m, sorry,
  intro,
  have o₁ : m_n ≥ 0 := nat.le_of_lt_succ ᾰ,
  cases o₁, focus {
    have o₂ : 1 = nat.zero.succ := rfl,
    exact Exists.intro 0 o₂,
  }, focus {
    have o₃ : 0 < o₁_b.succ := nat.lt_succ_of_le o₁_ᾰ, 
    have o₄ : ∀ (s : ℕ), o₁_b.succ = s.succ → 
    ∃ (n : ℕ), o₁_b.succ.succ = n.succ := by {
      intro, intro,
      have o₅ : o₁_b.succ.succ = s.succ.succ := congr_arg nat.succ ᾰ_1,
      exact Exists.intro s.succ o₅,
    },
    exact exists.elim (m_ih o₃) o₄,
  }
}

def axiom_peano_3 {a : ℕ} : a.succ ≠ 0 := sorry

def axiom_peano_4 {a b : ℕ} : a.succ = b.succ → a = b := sorry

def axiom_nat_add_zero {a : ℕ} : 0 + a = a := sorry

def axiom_nat_add_succ {a b : ℕ} : a.succ + b = (a+b).succ := sorry

lemma nat_add_cancel {a b c: ℕ} : a + b = a + c → b = c := by {
  intro, induction a, focus {
    rw axiom_nat_add_zero at ᾰ,
    rw axiom_nat_add_zero at ᾰ,
    exact ᾰ,
  }, focus {
    rw axiom_nat_add_succ at ᾰ,
    rw axiom_nat_add_succ at ᾰ,
    have o₁ : a_n + b = a_n + c := axiom_peano_4 ᾰ,
    exact a_ih o₁,
  },
} 

lemma l_007 {a b : ℕ} :
a + b = 0 ↔ a = 0 ∧ b = 0 := by {
  fconstructor, focus {
    intro,
    induction a, focus {
      rw axiom_nat_add_zero at ᾰ,
      exact ⟨refl 0, ᾰ⟩,
    }, focus {
      rw axiom_nat_add_succ at ᾰ,
      have o₁ : (a_n + b).succ = 0 → false := axiom_peano_3,
      exact false.elim (o₁ ᾰ),
    },
  }, focus {
    intro,
    cases ᾰ,
    rw ᾰ_left,
    rw ᾰ_right,
  },
} 

-- Exercise 2.3.2
def e_2_3_2 {m n : ℕ} : m * n = 0 ↔ m = 0 ∨ n = 0 := by {
  fconstructor,
  focus {
    intro,
    have o₁ : m = 0 ∨ m > 0 := by sorry,
    cases o₁, 
    focus {
      exact or.inl o₁,
    }, focus {
      have o₃ : ∀ s : ℕ, m = s.succ → m = 0 ∨ n = 0 := by {
        intro, intro,
        rw ᾰ_1 at ᾰ,
        rw axiom_nat_mul_succ at ᾰ,
        have o₄ : n = 0 ∨ n ≠ 0 := em (n = 0),
        cases o₄, focus {
          exact or.inr o₄,
        },
        have o₅ : n = 0 := and.elim_right ((iff.elim_left l_007) ᾰ),
        exact false.elim (o₄ o₅),
      },
      exact exists.elim (l_006 m o₁) o₃,
    },
  }, focus {
    intro,
    cases ᾰ, focus {
      rw ᾰ,
      exact axiom_nat_mul_zero,
    }, focus {
      rw ᾰ, rw e_2_3_1,
      exact axiom_nat_mul_zero,
    },
  },
}

def nat_add_assoc {a b c : ℕ} : a + b + c = a + (b + c) := sorry

def nat_add_comm {a b : ℕ} : a + b = b + a := sorry

-- Proposition 2.3.4
-- Left Distributivity
def p_2_3_4_left {a b c : ℕ} : a*(b+c) = a*b + a*c := by {
  induction a, focus {
    rw axiom_nat_mul_zero,
    rw axiom_nat_mul_zero,
    rw axiom_nat_mul_zero,
  }, focus {
    rw axiom_nat_mul_succ,
    rw axiom_nat_mul_succ,
    rw axiom_nat_mul_succ,
    rw a_ih, 
    have o₁ : a_n * b + a_n * c + (b + c) = a_n * b + b + (a_n * c + c) := by {
      rw nat_add_assoc,
      have oo₁ : a_n * c + (b + c) = (b + c) + a_n * c := nat_add_comm,
      rw oo₁,
      have oo₂ : b + c + a_n * c = b + (c + a_n * c) := nat_add_assoc,
      rw oo₂,
      have oo₃ : c + a_n * c = a_n * c + c := nat_add_comm,
      rw oo₃,
      rw ← nat_add_assoc,
    }, exact o₁,
  },
}

-- Right Distributivity
def p_2_3_4_right {a b c : ℕ} : (b+c)*a = b*a + c*a := by {
  rw e_2_3_1,
  rw p_2_3_4_left,
  rw e_2_3_1,
  have o₁ : a * c = c * a := e_2_3_1,
  rw o₁,
}

-- Exercise 2.3.3 / Proposition 2.3.5
def e_2_3_3 {a b c : ℕ} : a * b * c = a * (b * c) := by {
  induction a, focus {
    rw axiom_nat_mul_zero,
    rw axiom_nat_mul_zero,
    rw axiom_nat_mul_zero,
  }, focus {
    rw axiom_nat_mul_succ,
    rw axiom_nat_mul_succ,
    rw p_2_3_4_right,
    rw a_ih,
  }
}

def geq_definition {a b : ℕ} : a ≥ b ↔ ∃ r : ℕ, a = b + r := sorry

lemma l_008 {a : ℕ} : a ≥ 0 := by {
  have o₁ : a = 0 + a := by rw axiom_nat_add_zero,
  have o₂ : ∃ i : ℕ, a = 0 + i := Exists.intro a o₁,
  exact iff.elim_right geq_definition o₂,
} 

def gt_definition {a b : ℕ} : a > b ↔ a ≥ b ∧ a ≠ b := sorry

lemma l_009 {a b : ℕ} : a > b ↔ ∃ r : ℕ, r ≠ 0 ∧ a = b + r := sorry

lemma l_010 {a b : ℕ} : a > b → a.succ > b.succ := by {
  intro, 
  have o₂ : ∃ r : ℕ, r ≠ 0 ∧ a = b + r := (iff.elim_left l_009) ᾰ,
  have o₃ : ∃ r : ℕ, r ≠ 0 ∧ a.succ = b.succ + r := by {
    cases o₂ with x px, 
    cases px, existsi x,
    have oo₁ : a.succ = b.succ + x := sorry,
    exact and.intro px_left oo₁,
  },
  exact (iff.elim_right l_009) o₃,
}

lemma l_011 {a b c : ℕ} (h : a > b) :
a + c > b + c := by {
  induction c, focus {
    rw nat_add_comm,
    rw axiom_nat_add_zero,
    rw nat_add_comm,
    rw axiom_nat_add_zero,
     exact h,
  }, focus {
    rw nat_add_comm,
    rw axiom_nat_add_succ,
    have o₁ : b + c_n.succ = c_n.succ + b := nat_add_comm,
    rw o₁,
    rw axiom_nat_add_succ,
    rw nat_add_comm at c_ih,
    have o₂ : b + c_n = c_n + b := nat_add_comm,
    rw o₂ at c_ih,
    exact l_010 c_ih,
  },
}



lemma l_012 {a b c d : ℕ} (h₁ : a > b) (h₂ : c > d) :
a + c > b + d := by {
  have o₁ : a + c > b + c := l_011 h₁,
  have o₂ : c + b > d + b := l_011 h₂,
  rw nat_add_comm at o₂,
  transitivity, exact o₁,
  have o₃ : d + b = b + d := nat_add_comm,
  rw o₃ at o₂,
  apply o₂,
}

lemma l_013 {a : ℕ} : a * 1 = a := by {
  rw e_2_3_1,
  rw axiom_nat_mul_succ,
  rw axiom_nat_mul_zero,
  rw axiom_nat_add_zero,
}

-- Proposition 2.3.6
def p_2_3_6 {a b c : ℕ} (h₁: a > b) (h₂ : c > 0) :
a*c > b*c := by {
  induction c,
  focus {
    exfalso,
    have o₁ : 0 > 0 → false := lt_asymm h₂,
    exact o₁ h₂,
  },
  focus {
    have o₁ : c_n ≥ 0 := l_008, clear h₂,
    have o₂ : c_n > 0 ∨ c_n = 0 := sorry,
    clear o₁,
    cases o₂, focus {
      have oo₁ : a * c_n > b * c_n := c_ih o₂,
      clear c_ih o₂,
      rw e_2_3_1,
      have oo₂ : b * c_n.succ = c_n.succ * b := e_2_3_1,
      rw oo₂, clear oo₂,
      rw axiom_nat_mul_succ,
      rw axiom_nat_mul_succ,
      rw e_2_3_1,
      have oo₂ : c_n * b = b * c_n := e_2_3_1,
      rw oo₂, clear oo₂,
      exact l_012 oo₁ h₁,
    }, focus {
      clear c_ih,
      rw o₂,
      rw l_013,
      rw l_013,
      exact h₁,
    },
  },
} 


def nat_order_trichotomy {a b : ℕ} : a > b ∨ b > a ∨ b = a := sorry

-- Corollary 2.3.7
def c_2_3_7 {a b c : ℕ} (h₁ : a*c = b*c) (h₂ : c > 0) : 
a = b := by {
  have o₁ : a > b ∨ b > a ∨ b = a := nat_order_trichotomy,
  cases o₁, focus {
    exfalso,
    have oo₁ : a*c > b*c := p_2_3_6 o₁ h₂,
    have oo₂ : a*c ≠ b*c := and.elim_right ((iff.elim_left gt_definition) oo₁),
    exact oo₂ h₁,
  }, focus {
    cases o₁, focus {
      exfalso,
      have oo₁ : b*c > a*c := p_2_3_6 o₁ h₂,
      have oo₂ : b*c ≠ a*c := and.elim_right ((iff.elim_left gt_definition) oo₁),
      exact oo₂ (eq.symm h₁),},
    focus {
      symmetry, exact o₁,
    },
  },
}


-- Exercise 2.3.5 // Proposition 2.3.9
def e_2_3_5 {n q : ℕ} (h₁ : q ≠ 0) :
∃ m r : ℕ, r ≥ 0 ∧ q > r ∧ n = m * q + r := by {
  induction n, 
  focus {
    have o₁ : q > 0 := nat.pos_of_ne_zero h₁,
    have o₂ : 0 ≥ 0 := rfl.ge,
    existsi 0, existsi 0,
    split, exact o₂, split,
    exact o₁,
    rw axiom_nat_mul_zero,
    }, focus {
    cases n_ih with x hx,
    cases hx with y hxy,
    cases hxy, cases hxy_right,
    have o₁ : n_n.succ = (y + x * q).succ := by {
      rw nat_add_comm at hxy_right_right,
      rw hxy_right_right,
    },
    rw ← axiom_nat_add_succ at o₁,
    rw nat_add_comm at o₁,
    have o₂ : q ≥ y.succ := by {
      exact hxy_right_left, -- doesn't count. needs explicit proof.
    },
    have o₃ : q > y.succ ∨ q = y.succ := sorry,
    clear o₂, clear hxy_right_left hxy_left,
    cases o₃, focus {
      have oo₁ : y.succ ≥ 0 := l_008,
      existsi x, existsi y.succ,
      exact ⟨ oo₁, ⟨ o₃, o₁ ⟩⟩,
    },
    focus {
      rw o₃,
      rw o₃ at o₁, clear o₃ hxy_right_right,
      rw ← axiom_nat_mul_succ at o₁,
      have oo₁ : x.succ * y.succ = x.succ * y.succ + 0 := rfl,
      rw oo₁ at o₁, 
      have o₂ : 0 ≥ 0 := rfl.ge,
      have o₃ : y.succ > 0 := nat.succ_pos y,
      existsi x.succ, existsi 0,
      exact ⟨ o₂, ⟨ o₃, o₁ ⟩⟩,
    }, 
  },
}

-- Exercise 2.3.4
def e_2_3_4 {a b : ℕ} : (a + b)*(a + b) = a*a + 2*a*b + b*b := by {
  rw p_2_3_4_left,
  rw p_2_3_4_right,
  rw p_2_3_4_right,
  rw ← nat_add_assoc,
  have o₁ : b * a = a * b := e_2_3_1,
  rw o₁,
  have o₂ : a * b + a * b = 2 * a * b := by {
    have oo₁ : 1*(a*b) + 1*(a*b) = (1+1)*(a*b) := by rw ← p_2_3_4_right,
    rw e_2_3_1 at oo₁,
    rw l_013 at oo₁,
    rw oo₁,
    have oo₂ : 1 + 1 = 2 := rfl,
    rw oo₂,
    rw e_2_3_3,
  },
  rw ← o₂,
  have o₃ : a * a + (a * b + a * b) = a * a + a * b + a * b := by rw ← nat_add_assoc,
  rw o₃,
}