import tactic.basic
import data.nat.basic

open nat

namespace integer 

inductive int : Type 
| mk (a : ℕ) (b : ℕ) : int

notation `ℤ*` := int 
notation `[` a ` — ` b `]` := int.mk a b 

def left : ℤ* → ℕ :=
λ (i : ℤ*), int.rec_on i (λ a, λ b, a)

def right : ℤ* → ℕ :=
λ (i : ℤ*), int.rec_on i (λ a, λ b, b)

def equality {a b : ℤ*} : 
left a + right b = left b + right a ↔
a = b := sorry

def negation : ℤ* → ℤ* := 
λ (i : ℤ*), [right i — left i]

notation `-`i := negation i

example : [3 — 5] = -([5 — 3]) := by {exact rfl}


theorem t_1 (i : ℤ*) : i = [left i — right i] :=
int.rec_on i 
  (λ a, λ b,
  have o₁ : [a — b] = [left [a — b] — right [a — b]] := rfl, 
o₁)

example : [3 — 5] = [left [3 — 5] — right [3 — 5]] := by {exact rfl}

variable a : ℤ*
example : a = [left a — right a] := by {exact t_1 a,}


end integer

open integer 

-- Exercise 4.1.1 i, proof of reflexivity
def e_4_1_1_i {a : ℤ*} : a = a := by {
  have o₁ : left a + right a = left a + right a := rfl,
  exact equality.elim_left o₁,
}

-- Exercise 4.1.1 ii, proof of symmetry
def e_4_1_1_ii {a b : ℤ*} : a = b → b = a := by {
  intro h₁,
  have o₁ : left a + right b = left b + right a := 
  iff.elim_right equality h₁,
  have o₂ : left b + right a = left a + right b := 
  eq.symm o₁,
  exact iff.elim_left equality o₂,
}

-- Lemma 4.1.5, the trichotomy of integers

lemma l_1 {a b : ℕ} (h₁ : a < b) : 
∃ (c : ℕ), [a — b] = [0 — c.succ] := by {
    have o₁ : ∃ (c : ℕ), b = a + c.succ := exists_eq_add_of_lt h₁,
    clear h₁,
    cases o₁ with c h₁,
    have o₂ : a + c.succ = b := h₁.symm,
    clear h₁,
    have o₃ : b = 0 + b := by {symmetry, exact zero_add b},
    rw o₃ at o₂, clear o₃,
    have o₄ : [a — b] = [0 — c.succ] := equality.elim_left o₂,
    existsi c, exact o₄
}

lemma l_2 {i : ℤ*} (h₁ : left i < right i) :
∃ (c : ℕ), i = [0 — c.succ] := by {
  have o₁ : i = [left i — right i] := t_1 i,
  rw o₁, clear o₁, 
  exact l_1 h₁,
}

lemma l_3 (i : ℤ*) : -(-i) = i := 
integer.int.rec_on i (λ a, λ b, refl [a — b])

def trichotomy_of_integers_i (i : ℤ*) :
  i = [0 — 0] 
∨ (∃ (c : ℕ), i = [c.succ — 0]) 
∨ (∃ (c : ℕ), i = [0 — c.succ]) := by {
  let l := left i,
  let r := right i,
  have o₁ : l < r ∨ l = r ∨ r < l := lt_trichotomy l r,
  cases o₁, focus {
    have o₂ : ∃ (c : ℕ), i = [0 — c.succ] := l_2 o₁,
    finish,
  }, focus {
    cases o₁, focus {
      sorry, -- elaborating on this case wouldn't be particularly insightful 
    }, focus {
      let q := -i,
      have o₂ : ∃ (c : ℕ), q = [0 — c.succ] := l_2 o₁,
      cases o₂ with c h₁,
      have o₂ : -q = -([0 — c.succ]) := congr_arg negation h₁,
      clear h₁,
      have o₃ : -([0 — c.succ]) = [c.succ — 0] := rfl, rw o₃ at o₂, clear o₃,
      have o₄ : -q = i := l_3 i,rw o₄ at o₂, clear o₄,
      have o₅ : ∃ (c : ℕ), i = [c.succ — 0] := by {existsi c, exact o₂},
      finish, 
    },
  },
}

lemma l_4 {i : ℤ*} (h₁ : i = [0 — 0]) : ¬∃ (c : ℕ), i = [c.succ — 0] := by {
      intro h₂,
      cases h₂ with c h₃,
      rw h₃ at h₁, clear h₃,
      have o₁ : c.succ + 0 = 0 + 0 := equality.elim_right h₁, clear h₁,
      simp at *, exact o₁,
}

lemma l_5 {i : ℤ*} (h₁ : i = [0 — 0]) : ¬∃ (c : ℕ), i = [0 — c.succ] := by {
      intro h₂,
      cases h₂ with c h₃,
      rw h₃ at h₁, clear h₃,
      have o₁ : 0 + 0 = 0 + c.succ := equality.elim_right h₁, clear h₁,
      let o₂ := eq.symm o₁,
      simp at *, exact o₂,
}

def trichotomy_of_integers_ii (i : ℤ*) :
  i = [0 — 0] → 
  (¬(∃ (c : ℕ), i = [c.succ — 0]) 
  ∧ ¬(∃ (c : ℕ), i = [0 — c.succ])) := by {
    intro h₁, 
    split, focus {
      exact l_4 h₁,
    }, focus {
      exact l_5 h₁,
    },
  } 

def trichotomy_of_integers_iii (i : ℤ*) :
  (∃ (c : ℕ), i = [c.succ — 0]) → 
  (¬ (i = [0 — 0])
  ∧ ¬(∃ (c : ℕ), i = [0 — c.succ])) := by {
    intro h₁, 
    split, focus {
      intro h₂,
      cases h₁ with c h₃,
      rw h₃ at h₂, clear h₃,
      have o₁ : c.succ + 0 = 0 + 0 := equality.elim_right h₂,
      simp at *, exact h₂,
    }, focus {
      intro h₂,
      cases h₁ with c₁ h₃,
      cases h₂ with c₂ h₄,
      rw h₄ at h₃, clear h₄,
      have o₁ : 0 + 0 = c₁.succ + c₂.succ := equality.elim_right h₃, 
      simp at *,
      exact h₃,
    },
  } 

def trichotomy_of_integers_iiii (i : ℤ*) : 
  (∃ (c : ℕ), i = [0 — c.succ]) → 
  (¬ (i = [0 — 0])
  ∧ ¬(∃ (c : ℕ), i = [c.succ — 0])) := by {
    intro h₁, 
    split, focus {
      intro h₂,
      cases h₁ with c h₃,
      rw h₃ at h₂, clear h₃,
      have o₁ : 0 + 0 = 0 + c.succ := equality.elim_right h₂,
      simp at *, exact h₂,
    }, focus {
      intro h₂,
      cases h₁ with c₁ h₃,
      cases h₂ with c₂ h₄,
      rw h₄ at h₃, clear h₄,
      have o₁ : c₂.succ + c₁.succ = 0 + 0 := equality.elim_right h₃, 
      simp at *,
      exact h₃,
    },
  }


-- Set up multiplication
def mult (a b : ℤ*) : ℤ* := by {
  let l := (left a * left b) + (right a * right b),
  let r := (left a * right b) + (right a * left b),
  exact [l — r],
}

infix ` x `:50 := mult

#reduce [0 — 3] x [5 — 0] -- [0 — 15]




